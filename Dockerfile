# Elixir for GitLab CI 2020-10-29 by @k33g | on gitlab.com 
FROM elixir:1.10.4-alpine

LABEL maintainer="@k33g_org"
LABEL authors="@k33g_org"
LABEL version="1.0"

# 🚧 wip
RUN apk --update add --no-cache curl

COPY elixir_run.sh /usr/local/bin/elixir_run
RUN chmod +x /usr/local/bin/elixir_run

CMD ["/bin/sh"]

